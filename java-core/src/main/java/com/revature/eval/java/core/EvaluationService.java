package com.revature.eval.java.core;

import java.io.File;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class EvaluationService {

	/**
	 * 1. Without using the StringBuilder or StringBuffer class, write a method that
	 * reverses a String. Example: reverse("example"); -> "elpmaxe"
	 * 
	 * @param string
	 * @return
	 */
	public String reverse(String string) {
		char[] reversed = new char[string.length()];
		for (int i = reversed.length - 1, j=0; i >= 0; i--, j++) {
			reversed[j] = string.charAt(i);
		}
		return new String(reversed);
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 * 
	 * @param phrase
	 * @return
	 */
	public String acronym(String phrase) {
		//System.out.println("HERe is me debug");
		
		//phrase=phrase.replaceAll("[^\\w\\s-]", "");
		phrase=phrase.replaceAll("[^a-zA-Z0-9]", " ");
		phrase = phrase.replaceAll("\\s+", " ");


		//System.out.println(phrase);
		String []y=phrase.split(" ");
		String c="";
		for(int i=0;i<y.length;i++)
		{
			//System.out.println(y[i]+" "+y.length);
			//System.out.println(y[i].charAt(0));

			c=c+y[i].charAt(0);
		}
		//System.out.println(c.toUpperCase());
		return c.toUpperCase();
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 */
	static class Triangle {
		private double sideOne;
		private double sideTwo;
		private double sideThree;

		public Triangle() {
			super();
		}

		public Triangle(double sideOne, double sideTwo, double sideThree) {
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
		}

		public double getSideOne() {
			return sideOne;
		}

		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}

		public double getSideTwo() {
			return sideTwo;
		}

		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}

		public double getSideThree() {
			return sideThree;
		}

		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral() {
			// TODO Write an implementation for this method declaration
			if(sideOne==sideTwo &&sideOne==sideThree &&sideTwo==sideThree)
			{
				return true;
			}
			
				return false;
			
			
			
		}

		public boolean isIsosceles() {
			// TODO Write an implementation for this method declaration
			if(sideOne==sideTwo && sideTwo!=sideThree)
			{
				return true;
			}
			else if(sideOne==sideThree&&sideTwo!=sideThree)
			{
				return true;
			}
			else if (sideTwo==sideThree &&sideOne!=sideThree)
			{
				return true;
			}
				
		
			return false;
		}

		public boolean isScalene() {
			// TODO Write an implementation for this method declaration
			if(sideOne!=sideTwo && sideOne!=sideThree && sideTwo!=sideThree) {
				
				return true;
				
			}
			
			
			return false;
		}

	}

	/**
	 * 4. Given a word, compute the scrabble score for that word.
	 * 
	 * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
	 * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
	 * "cabbage" should be scored as worth 14 points:
	 * 
	 * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
	 * point for E And to total:
	 * 
	 * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
	 * 
	 * @param string
	 * @return
	 */
	public int getScrabbleScore(String string) {
		// TODO Write an implementation for this method declaration
		int sum=0;
		
		string=string.toUpperCase();
		
		for(int i=0;i<string.length();i++)
		{
			if(string.charAt(i)=='K')
			{
				sum=sum+5;
			}
			else if(string.charAt(i)=='J'||string.charAt(i)=='X')
			{
				sum=sum+8;
			}
			else if(string.charAt(i)=='Z'||string.charAt(i)=='Q')
			{
				sum=sum+10;
			}
			else if(string.charAt(i)=='D'||string.charAt(i)=='G')
			{
				sum=sum+2;
			}
			else if (string.charAt(i)=='B'||string.charAt(i)=='C'||string.charAt(i)=='M'||string.charAt(i)=='P')
			{
				sum=sum+3;
			}
			else if (string.charAt(i)=='F'||string.charAt(i)=='H'||string.charAt(i)=='V'||string.charAt(i)=='W'||string.charAt(i)=='Y')
			{
				sum=sum+4;
			}
			else
			{
				sum=sum+1;
			}
				
		}
	
		return sum;
	}

	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 * 
	 * The North American Numbering Plan (NANP) is a telephone numbering system used
	 * by many countries in North America like the United States, Canada or Bermuda.
	 * All NANP-countries share the same international country code: 1.
	 * 
	 * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
	 * Area code, commonly known as area code, followed by a seven-digit local
	 * number. The first three digits of the local number represent the exchange
	 * code, followed by the unique four-digit number which is the subscriber
	 * number.
	 * 
	 * The format is usually represented as
	 * 
	 * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
	 * from 0 through 9.
	 * 
	 * Your task is to clean up differently formatted telephone numbers by removing
	 * punctuation and the country code (1) if present.
	 * 
	 * For example, the inputs
	 * 
	 * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
	 * the output
	 * 
	 * 6139950253
	 * 
	 * Note: As this exercise only deals with telephone numbers used in
	 * NANP-countries, only 1 is considered a valid country code.
	 */
	public String cleanPhoneNumber(String string) {
		// TODO Write an implementation for this method declaration
		
		String t2 = string;
		t2 = t2.replaceAll("\\W+","");
		//System.out.println(t2);
		//System.out.println(t2.length());
		
		for(int i=0;i<t2.length();i++)
		{
			if(!Character.isDigit(t2.charAt(i)))
			{
				throw new IllegalArgumentException();
			}
		}
		if(t2.length()<=11) {
			int c=t2.indexOf("1");
			String sum=t2.substring(c+1, t2.length());
			t2=sum;
		}
		else {
			//System.out.println("asi");
			
			throw new IllegalArgumentException();
			
		}
		
		
		//return java.lang.IllegalArgumentException;
		
		



		return t2;
	}

	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 * 
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 * 
	 * @param string
	 * @return
	 */
	public Map<String, Integer> wordCount(String string) {
		// TODO Write an implementation for this method declaration
		
		string=string.replaceAll("[^\\w\\s\\-_]", " ");

		String []y=string.split(" ");
		//Map<String,Integer> sum=new Map<String, Integer>();
		//System.out.println(string);
		//System.out.println(y[0]);
		
		//HashMap <String,Integer> st=new HashMap<String, Integer>();
		LinkedHashMap<String, Integer> hm= new LinkedHashMap<String, Integer>();
		//System.out.println(string);
		for(int i=0;i<y.length;i++)
		{
			//System.out.println(y[i]);
			if(!hm.containsKey(y[i]))
			{
				hm.put(y[i], 1);
			}
			else
			{
				hm.put(y[i], hm.get(y[i])+1);
			}
			
			//System.out.println(hm);

			
		}
		//st=hm;
		//System.out.println(hm);

		return hm;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 * 
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 * 
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 * 
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 * 
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 * 
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 * 
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 * 
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 * 
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 * 
	 */
	static class BinarySearch<T> {
		private List<T> sortedList;

		public int indexOf(T t)  {
			// TODO Write an implementation for this method declaration
			int n=sortedList.size(); 

			int res=0;
			int low=1;
		

			int x=0;
			
			
				
				if(!t.getClass().getSimpleName().equals("Integer"))
				{
					x=Integer.parseInt((String) t);
				}
				else
				{
					x=(int)t;
				}
							      

		


			int high=n;
			int c=0;
			while(c==0)
			{

				
		
				if(high<low)
				{
					c++;
				}

			
				
				int mid=low+(high-low)/2;
				

				if(sortedList.get(mid).getClass().getSimpleName().equals("Integer")) {
					
					if((int)sortedList.get(mid)<x)
					{

						low=mid+1;
					}
					
					if((int)sortedList.get(mid)>x)
					{
						high=mid-1;
					}
					
					if((int)sortedList.get(mid)==x)
					{
						c++;
						res=mid;
					}
					
					
					
				}
				else
				{
					if(Integer.parseInt((String) sortedList.get(mid))<x)
					{
						//System.out.println("asi");

						low=mid+1;
					}
					
					if(Integer.parseInt((String) sortedList.get(mid))>x)
					{
						high=mid-1;
					}
					
					if(Integer.parseInt((String) sortedList.get(mid))==x)
					{
						c++;
						res=mid;
					}
					
					
				}
				
		
				//break;
				//System.out.println("asi");

				
			
			
			}

			//System.out.println((Integer)res);
			return (Integer)res;
		}

		public BinarySearch(List<T> sortedList) {
			super();
			this.sortedList = sortedList;
		}

		public List<T> getSortedList() {
			return sortedList;
		}

		public void setSortedList(List<T> sortedList) {
			this.sortedList = sortedList;
		}

	}

	/**
	 * 8. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		// TODO Write an implementation for this method declaration
		String sum="";
		string=string.toLowerCase();
		//string=string.replace(" ", "");
		String []y=string.split(" ");
		
		
		if(string.charAt(0)=='a'||string.charAt(0)=='e'||string.charAt(0)=='i'||string.charAt(0)=='o'||string.charAt(0)=='u')
		{
			sum=string+"ay";
		}
		else
		{
			sum=string.substring(1,string.length());
			sum=sum+string.valueOf(string.charAt(0))+"ay";
		}
		//System.out.println(string);
		//System.out.println(sum);
		
		return sum;
	}

	/**
	 * 9. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		// TODO Write an implementation for this method declaration
		String a=""+input;
		double sq=a.length();
		double sum=0;
		for(int i=0;i<a.length();i++)
		{
			sum=sum+Math.pow(Integer.parseInt(String.valueOf(a.charAt(i))), sq);
		}
		
	
		if((int)sum==input) {
			
		
			return true;
		}
		
		return false;
	}

	/**
	 * 10. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		// TODO Write an implementation for this method declaration
		long n=l;
		//System.out.println(n);
		ArrayList<Long> hm =new ArrayList<Long>(); 
		  for (int i = 2; i <= n; i++) {
			   while (n % i == 0) {
			    hm.add((long) i);
			    n /= i;
			   }
			  }
	
    
		return hm;
	}

	/**
	 * 11. Create an implementation of the rotational cipher, also sometimes called
	 * the Caesar cipher.
	 * 
	 * The Caesar cipher is a simple shift cipher that relies on transposing all the
	 * letters in the alphabet using an integer key between 0 and 26. Using a key of
	 * 0 or 26 will always yield the same output due to modular arithmetic. The
	 * letter is shifted for as many values as the value of the key.
	 * 
	 * The general notation for rotational ciphers is ROT + <key>. The most commonly
	 * used rotational cipher is ROT13.
	 * 
	 * A ROT13 on the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
	 * stronger than the Atbash cipher because it has 27 possible keys, and 25
	 * usable keys.
	 * 
	 * Ciphertext is written out in the same formatting as the input including
	 * spaces and punctuation.
	 * 
	 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
	 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
	 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
	 * quick brown fox jumps over the lazy dog.
	 */
	static class RotationalCipher {
		private int key;

		public RotationalCipher(int key) {
			super();
			this.key = key;
		}

		public String rotate(String string) {
			// TODO Write an implementation for this method declaration
			
			//string=a;
			
			String sum="";
			int k;
			char e = 0;
			//System.out.println(string);
			for(int i=0;i<string.length();i++)
			{
				int c=(int)string.charAt(i);
				//System.out.println(c);
				if((int)string.charAt(i)<97) {
					
					if((int)string.charAt(i)<65)
					{
						sum=sum+string.charAt(i);
					}
					else if(c+13<91)
					{
						 e=(char) (c+13);
						 sum=sum+e;
					}
					else if (c+13>91)
					{
						   e=(char) (c-13);
							 sum=sum+e;
					}
					else
					{
						sum=sum+string.charAt(i);

					}
					
					
					
					
					
					
				}
				else if(c+13<=122)
				{
					// k=(int)string.charAt(i);
					 e=(char) (c+13);
					 sum=sum+e;
				}
				
				
				else {
					//k=(int)string.charAt(i);
				    e=(char) (c-13);
					 sum=sum+e;

				}
				
			}
			
			//System.out.println(sum);
			//System.out.println((int)' ');
	
			return sum;
		}

	}

	/**
	 * 12. Given a number n, determine what the nth prime is.
	 * 
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 * 
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 * 
	 * @param i
	 * @return
	 */
	public int calculateNthPrime(int i)  {
		// TODO Write an implementation for this method declaration
		//System.out.println(i);
		
		
			if(i==0) {
				
				
	     throw new IllegalArgumentException();
			
			 
		}
		int a=0;
		int nth=i;
		int num, count,c;
	    num=1;
	    count=0;
	 
	    while (count < nth){
	      num=num+1;
	      for ( c = 2; c <= num; c++){ //Here we will loop from 2 to num
	        if (num % c == 0) {
	          break;
	        }
	      }
	      if ( c == num){//if it is a prime number
	        count = count+1;
	      }
	      
	      //System.out.println(num);
		
		//return num;
	}
		return num;
	}
	/**
	 * 13 & 14. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	static class AtbashCipher {

		/**
		 * Question 13
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			// TODO Write an implementation for this method declaration
			//string=string.replace(" ", "");
			//System.out.println(string);
			string=string.toLowerCase().replaceAll("[^\\w\\d]", "");
			//System.out.println(string);

			String a="abcdefghijklmnopqrstuvwxyz";
			String b="zyxwvutsrqponmlkjihgfedcba";
			String sum="";
			//System.out.println(sum);
			int grp=0;
			
			for(int i=0;i<string.length();i++)
			{
				
				if(!Character.isDigit(string.charAt(i)))
						{
					
					int ind=a.indexOf(string.charAt(i));
					char c=b.charAt(ind);
					
						if(grp==5)
						{
							sum=sum+" ";
							grp=0;
						}
						grp++;
						
						sum=sum+""+c;
					
						}
				else
				{
					sum=sum+Integer.parseInt(String.valueOf(string.charAt(i)));
					grp++;

				}
			
				
				
				
				
				
			}
		//	System.out.println(sum);
			
			return sum;
		}

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string) {
			// TODO Write an implementation for this method declaration
			
			string=string.toLowerCase().replaceAll("[^\\w\\d]", "");
			//System.out.println(string);

			String a="abcdefghijklmnopqrstuvwxyz";
			String b="zyxwvutsrqponmlkjihgfedcba";
			String sum="";
			//System.out.println(sum);
			int grp=0;
			
			for(int i=0;i<string.length();i++)
			{
				
				if(!Character.isDigit(string.charAt(i)))
						{
					
					int ind=b.indexOf(string.charAt(i));
					char c=a.charAt(ind);
					
						sum=sum+""+c;
					
						}
				else
				{
					sum=sum+Integer.parseInt(String.valueOf(string.charAt(i)));
				}
			
				
				
				
				
				//System.out.println(sum);
			}
			return sum;
		}
	}

	/**
	 * 15. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string) {
		// TODO Write an implementation for this method declaration
		int sum=0;
		int isb=10;
		string=string.replace("-", "");
		//System.out.println(string);
	
		//string=string.replace("X", "10");
		for(int i=0;i<string.length();i++)
		{
			if((int)string.charAt(i)<58)
			{
				sum=sum+Integer.parseInt(string.valueOf(string.charAt(i)))*isb;
			}
			else  {
				
				if(string.charAt(i)=='X') {
					
					sum=sum+10;
				}
				else {
					
					//return false;
				}
			}
			
			isb--;
			
		}
		//System.out.println(sum);

		
		if(sum % 11==0)
		{
			return true;
		}
		else {
			return false;
		}
		
		
	}

	/**
	 * 16. Determine if a sentence is a pangram. A pangram (Greek: pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 * 
	 * The quick brown fox jumps over the lazy dog.
	 * 
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isPangram(String string) {
		// TODO Write an implementation for this method declaration
		
		String a="abcdefghijklmnopqrstuvwxyz";
		//System.out.println(string);
		//string=string.replace(" ","");
		

		//System.out.println(string);
       // int space=0;
        //string=string.replace(" ","");
		HashMap<Character, Integer> hm= new HashMap<Character, Integer>();

		for(int i=0;i<string.length();i++)
		{
			//int c=0;

			
			for(int j=0;j<a.length();j++)
			{
			
				
				if(a.charAt(j)==string.charAt(i))
				{
					//System.out.println(string);
					
					hm.put(a.charAt(j), 0);
					//a=a.replace(""+(a.charAt(j)), "");
					
					//System.out.println(a);

					//return false;
					//c++;
				}
				
				//System.out.println(hm);

				//System.out.println(string.charAt(i));
				//System.out.println(c);

			
			}
			
		
			
			
			
		}
		//System.out.println(string);
		//System.out.println(hm.size());
		if(hm.size()==26)
		{
			return true;
		}
		else
		{
			return false;
		}

			//return true;

		}
	

	/**
	 * 17. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param i
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int i, int[] set) {
		// TODO Write an implementation for this method declaration
		
		int sum=0;
		//ArrayList<Integer> hm= new ArrayList<Integer>();
		HashMap<Integer, Integer> hm=new HashMap<Integer, Integer>();
		//System.out.println("asi "+i);

		for(int c=0;c<set.length;c++)
		{
			int in=set[c];
			//System.out.println("nai "+in);
			for(int j=1;in*j<i;j++)
			{
				//hm.add(in*j);
				if(!hm.containsKey(in*j))
				{
					//System.out.println(in*j);
					hm.put(in*j, 0);
					sum=sum+in*j;
				}
				
				
				
				//System.out.println(hm);

			}
			//System.out.println(sum);
		}
		
		//hm.clear();
		return sum;
	}

	/**
	 * 18. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string) {
		// TODO Write an implementation for this method declaration
		int sum=0;
		boolean f=false;
		//System.out.println(string);
		string=string.replace(" ", "");
		//System.out.println(string);

		//string=string.substring(1, string.length());
		
		for(int i=0;i<string.length();i++)
		{
			if(!Character.isDigit(string.charAt(i)))
			{
				//System.out.println("asi");

				return false;
			}
			
			if(f)
			{
				int c=Integer.parseInt(string.valueOf(string.charAt(i)));
				//System.out.println(string.charAt(i));
				if(c*2>9)
				{
					c=c-9;
				}
				else
				{
					c=c*2;
				}
				
				sum=sum+c;
				
				f=false;
			}
			else {
				
				sum=sum+Integer.parseInt(string.valueOf(string.charAt(i)));
				
				f=true;
				
			}
			
			
		}
		//System.out.println(sum);

		if(sum%2==0)
		{
			return true;
		}
		else
		{
			//System.out.println("nai");

			return false;

		}
		
	}

	/**
	 * 19. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string) {
		// TODO Write an implementation for this method declaration
		
		string=string.replace("?", "");
		String []y=string.split(" ");
		int c=0;
		
		if(y[3].equals("plus"))
		{
			c=Integer.parseInt(y[2])+Integer.parseInt(y[y.length-1]);
			//System.out.println(c);
		}
		else if (y[3].equals("minus")) {
			
			c=Integer.valueOf(y[2])-Integer.valueOf(y[y.length-1]);
		}
		else if(y[3].equals("multiplied"))
		{
			c=Integer.valueOf(y[2])*Integer.valueOf(y[y.length-1]);
			//System.out.println(c);

			
		}
		else
		{
			
			c=Integer.parseInt(y[2])/Integer.parseInt(y[y.length-1]);

		}
		
			
			
		return c;
	}

}
